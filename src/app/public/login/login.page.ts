import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router";
import { AuthenticationService } from './../../services/authentication.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {


	constructor(private router: Router, private authenticationService: AuthenticationService) { }


  ngOnInit() {
  }


	login() {
		this.authenticationService.login()
		this.router.navigateByUrl("/members/home")		// this.router.navigate(['members', 'dashboard'])
	}

}
