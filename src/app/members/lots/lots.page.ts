import { Component, OnInit } from '@angular/core'
import { NavController } from '@ionic/angular'

import * as dayjs from 'dayjs'

//import { AlertService } from '../services/alert.service'
import { AuctionService } from '../../services/auction.service'

import { ILot } from '../../Interfaces';


@Component({
	selector: 'app-lots',
	templateUrl: './lots.page.html',
	styleUrls: ['./lots.page.scss'],
})

export class LotsPage implements OnInit {
	public lot: ILot
	private connection
	public messages = []		// public messages: Array<any> = []
	public message
	public bid: number = 0;

	constructor(public navCtrl: NavController, private auctionService: AuctionService) {
		this.lot = {
			bidHistory: null,
			currentPrice: null,
			endTime: null,
			id: '',
			name: '',
			picture: '',
			startPrice: null,
			// minBid: 100
		}
	}


	ngOnInit() {
		this.auctionService.getLot('62aee188-d0de-471a-8e2e-2918b0bfdd62').subscribe(data => {
			this.lot = data;
			this.connection = this.auctionService.getMessages().subscribe(bid => {
				const bidHistory = [bid, ...this.lot.bidHistory]
				this.lot.bidHistory = bidHistory.sort((a, b) =>  Number(b.amount) - Number(a.amount));
				this.lot.currentPrice = this.lot.bidHistory[0].amount
			})
		})
	}

	ngOnDestroy() {
		this.connection.unsubscribe()
	}


	placeBid(bid:number) {
		console.log("placeBid: ", bid)
		this.auctionService.sendMessage({ lotId: this.lot.id, amount: bid })
	}


	placeMinBidX(amount:number) {
		console.log("placeBid: ", amount * 100)
		this.auctionService.sendMessage({ lotId: this.lot.id, amount: this.lot.currentPrice + amount * 100 })
	}

}
