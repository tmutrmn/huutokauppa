import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotsPage } from './lots.page';

describe('LotsPage', () => {
  let component: LotsPage;
  let fixture: ComponentFixture<LotsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
