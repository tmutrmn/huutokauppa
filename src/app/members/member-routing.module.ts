import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
	{ path: '', redirectTo: "home", pathMatch: "full" },
	{ path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
	{ path: 'home', loadChildren: './home/home.module#HomePageModule' },
	{ path: 'list', loadChildren: './list/list.module#ListPageModule' },
	{ path: 'lots', loadChildren: './lots/lots.module#LotsPageModule' },
	{ path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MemberRoutingModule { }
