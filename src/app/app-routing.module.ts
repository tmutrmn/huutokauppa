import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [

  // { path: 'register', loadChildren: './public/register/register.module#RegisterPageModule' },
  {
    path: 'members',
    canActivate: [AuthGuard],
    loadChildren: './members/member-routing.module#MemberRoutingModule'
	},
	// { path: '', redirectTo: 'login', pathMatch: 'full' },
	{ path: '', redirectTo: 'login', pathMatch: 'full' },
	{ path: 'login', loadChildren: './public/login/login.module#LoginPageModule' },
	{ path: 'register', loadChildren: './public/register/register.module#RegisterPageModule' },
	{ path: '**', redirectTo: '' }
];


@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
