import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';


@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

	constructor(private router: Router, public authenticationService: AuthenticationService) {

	}


	canActivate(): boolean {
		if (!this.authenticationService.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
		return this.authenticationService.isAuthenticated()
  }

	/*
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
	}
	*/
}
