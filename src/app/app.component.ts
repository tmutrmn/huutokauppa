import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AuthenticationService } from './services/authentication.service'


@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html'
})

export class AppComponent {
	public appPages = [
		{
			title: 'Home',
			url: '/members/home',
			icon: 'home'
		},
		{
			title: 'Dashboard',
			url: '/members/dashboard',
			icon: 'dashboard'
		},
		{
			title: 'Lots',
			url: '/members/lots',
			icon: 'list'
		},
		{
			title: 'List',
			url: '/members/list',
			icon: 'list'
		},
		{
			title: 'Logout',
			url: '/members/logout',
			icon: 'log-out'
		},
	];

	private loggedIn:boolean = false;


	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private authenticationService: AuthenticationService
	) {
		this.initializeApp();
	}


	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
			/*
			this.authenticationService.authenticated.subscribe(state => {
        if (state) {
          this.router.navigate(['members', 'dashboard']);
        } else {
          this.router.navigate(['login']);
        }
			})
			*/
		});
	}
}
