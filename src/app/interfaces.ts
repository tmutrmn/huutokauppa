export interface ILot {
	bidHistory: any[];
	currentPrice: number;
	endTime: any;					// "2017-10-07T22:53:04.118Z"
	id: string;						// "62aee188-d0de-471a-8e2e-2918b0bfdd62"
	name: string;					// "Mini Cooper"
	picture: string;			// "http://lorempixel.com/128/128/transport/8/"
	startPrice: number;		//
}
