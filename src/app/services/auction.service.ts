import { Injectable } from '@angular/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';



const API_URL = 'http://localhost:8000/api/lots/'


@Injectable({
	providedIn: 'root'
})

export class AuctionService implements OnDestroy {		// OnInit,
	private observable: any
	private lotId: string =	'62aee188-d0de-471a-8e2e-2918b0bfdd62'


	constructor(public http: HttpClient, private socket: Socket) {

	}

	ngOnDestroy() {
		this.observable.unsubscribe()
	}


	sendMessage(msg) {
		if (!msg) return
		this.socket.emit('new_bid', { lotId: msg.lotId, amount: msg.amount })
	}


	getMessages() {
		this.observable = new Observable(observer => {		// const  observable
			this.socket.on('new_bid_placed', (data) => {
				console.log('new_bid_placed: ', data)
				observer.next(data)
			})
			this.socket.on('new_bid_error', (data) => {
				console.log('new_bid_error: ', data)
				observer.next(data)
			})
			return () => {
				this.socket.disconnect()
			}
		})
		return this.observable		// observable
	}




	getLot(id:string) {
		const url = API_URL + id;
		return this.fetchData(url)
	}


	private fetchData(url) {
		return this.http.get<any>(url)
			.pipe(
				tap(data => this.log('fetched data')),
				catchError(this.handleError('fetchData', null))
			);
	}


	/**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
	private handleError<T>(operation = 'operation', result?: T) {		// copied straight from ng docs
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error);		// log to console instead
			this.log(`${operation} failed: ${error.message}`);			// TODO: better job of transforming error for user consumption
			return of(result as T);		// Let the app keep running by returning an empty result.
		};
	}

	/*** Log service message... should implement a separate MessageService to handle these ***/
	private log(message: string) {
		console.log(`auction.service: ${message}`);
	}


}
