import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';


@Injectable({
	providedIn: 'root'
})

export class LoadingService {
	private loader;

	constructor(public loadingController: LoadingController) {
		this.loader = null
	}


	public async show() {
		this.loader = await this.loadingController.create({
			message: "Loading...",
		});
		return await this.loader.present();
	};

	public async hide() {
		if (this.loader != null) return await this.loader.dismiss();		// muista aina, liikenteessä, katsoa onko vaaraa eessä...
	};


	public async toggle() {
		await this.show();
		setTimeout(() => {
			this.hide();
		}, 2000);
	};

}
