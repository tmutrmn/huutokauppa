import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';



@Injectable({
	providedIn: 'root'
})

export class AlertService {

	constructor(public alertController: AlertController) { }


	public async showAlert(alertTitle: string, alertMessage: string, callback?: any) {
		const alert = await this.alertController.create({
			header: alertTitle,
			message: alertMessage,
			buttons: [
				{
					text: 'Cancel',
					handler: () => { }
				},
				{
					text: 'OK',
					handler: () => {
						if (callback) callback()
					}
				}
			]
		});
		return await alert.present();
	}

}
