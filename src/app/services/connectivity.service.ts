import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, of, Subject, throwError } from 'rxjs';

import { Platform } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';



@Injectable({
	providedIn: 'root'
})
export class ConnectivityService {
	private connectionObserver: any;
	public connection: any;

	public connectSubscription: any;
	public disconnectSubscription: any;
	public online: boolean;

	constructor(public platform: Platform, public alertCtrl: AlertController, public network: Network, public diagnostic: Diagnostic) {
		this.connectionObserver = null;
		this.connection = Observable.create(observer => {
			this.connectionObserver = observer;
		});
		this.online = this.getConnectionStatus();
	}


	public getConnectionStatus(): boolean {
		if (this.platform.is('cordova')) {
			if (this.network.type !== "unknown" && this.network.type !== "none")
				return true
			else
				return false
		} else {
			return navigator.onLine;		// this.status = navigator.onLine ? "Online" : "Offline";
		}
	}


	public connectionStatus(isOnline): string {
		if (isOnline) {
			return "Online";
		} else {
			return "Offline";
		}
	}


	public connectionHasChanged(isOnline) {		// Send that information to all the subscribers
		this.connectionObserver.next(isOnline);
	}


	private showSettings() {

		if (this.diagnostic.switchToMobileDataSettings) {
			this.diagnostic.switchToMobileDataSettings();
		} else {
			this.diagnostic.switchToSettings();
		}

	}


	public async noNetworkConnectionAlert() {
		const networkAlert = await this.alertCtrl.create({
			header: 'No Internet Connection',
			message: 'Please check your internet connection.',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
					handler: () => { }
				}, {
					text: 'Open Settings',
					handler: () => {
						networkAlert.onDidDismiss().then(() => {
							this.showSettings();
						})
					}
				}
			]
		});
		return await networkAlert.present();
	}





}
