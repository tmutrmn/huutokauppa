import { Platform } from '@ionic/angular'
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = 'auth-token';


@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

	authenticated = new BehaviorSubject(false);

  constructor(private storage: Storage, private platform: Platform) {
    this.platform.ready().then(() => {
      this.checkToken();
    });
	}


	/*
	checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticated.next(true);
      }
    })
	}
	*/

	checkToken() {
		this.storage.get(TOKEN_KEY).then(res => {
			const hasToken = !!res
			console.log('checkToken hasToken: ', hasToken)
			this.authenticated.next(hasToken)
		});
	}


	login() {
    return this.storage.set(TOKEN_KEY, 'Bearer 1234567').then(() => {
      this.authenticated.next(true)
    });
	}


	logout() {
		this.storage.get(TOKEN_KEY).then(res => {
			const hasToken = !!res
			console.log('logout hasToken: ', hasToken)
		})
		return this.storage.remove(TOKEN_KEY).then(() => {
			this.authenticated.next(false)
			console.log('logout authenticated: ', this.authenticated.value)
		});
	}


	isAuthenticated() {
    return this.authenticated.value
  }

}
