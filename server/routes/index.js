const router = require('express').Router()
const lots = require('../../data/lots.json')

const ERROR_404 = {
  status: 404,
  message: 'Not Found'
}


const getLotById = (lotId) => Promise.resolve(lots[0])


const placeBid = (bid, lot) => new Promise((resolve, reject) => {
  if (lot.bidHistory.some(({ amount }) => amount >= bid.amount)) {
    return reject(new Error('Bid Validation: New bid should be higher than the current highest bid'))
  }
	// lot.currentPrice = lot.startPrice + bid.amount
	lot.currentPrice = lot.startPrice + (bid.amount - lot.startPrice)
	console.log(lot.startPrice, lot.currentPrice, bid.amount)
  lot.bidHistory = [ bid, ...lot.bidHistory ]
  return resolve(bid)
})


router.get('/lots/:id', (req, res, next) => {
	console.log(req.params.id)
  getLotById(req.params.id)
    .then(lot => res.send(lot))
    .catch(err => next(ERROR_404))
})

module.exports = router
