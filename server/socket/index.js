const { Observable, of, from, fromEvent, throwError, empty } = require('rxjs')
const { tap, map, mergeAll, withLatestFrom, catchError } = require('rxjs/operators')
const crypto = require('crypto')
const errorHandler = require('./error-handler')
const lots = require('../../data/lots.json')



const getLotById = (lotId) => Promise.resolve(lots[0])		// lotId

const placeBid = (bid, lot) => new Promise((resolve, reject) => {
	console.log(bid.amount, lot)
  if (lot.bidHistory.some(({ amount }) => amount >= bid.amount)) {
		// console.log(amount, bid.amount)
    return reject(new Error('Bid Validation: New bid should be higher than the current highest bid'))
  }
	lot.currentPrice = lot.startPrice + (bid.amount - lot.startPrice)		// bid.amount
  lot.bidHistory = [ bid, ...lot.bidHistory ]
	return resolve(bid)
	//return reject(new Error('Bid Validation: New bid should be higher than the current highest bid'))
})



const getNumberOfSockets = (io) => {
  return Object.keys(io.sockets.sockets).length
}

const emitWatchCount = (io) => {
  io.emit('watch_count', getNumberOfSockets(io) - 1)
}

const generateName = (value) => crypto
      .createHash('md5')
      .update(value)
      .digest("hex")
      .substr(0, 12) + '...'



initConnection = (io) => (socket) => {
  const bidder = generateName(socket.id)
	emitWatchCount(io)

  const newBid$ = fromEvent(socket, 'new_bid').pipe(
		map(bid => Object.assign(bid, { bidder })),
		tap(bid => console.log('new_bid'))
	)

  const biddingForLot$ = newBid$.pipe(
    map(bid => from(getLotById(bid.lotId))),
		mergeAll(),
	)

  socket.on('disconnect', () => {
    emitWatchCount(io)
  })

	return biddingForLot$.pipe(
		withLatestFrom(newBid$, (lot, bid) => from(placeBid(bid, lot))),
		mergeAll(),
		catchError((err, caugth$) => {
			console.log('Returning caught observable');
			socket.emit('new_bid_error', { message: err.message })
			// return throwError(err);
			// return empty()
			// return of()
			return caugth$
    }),
		tap(bid => {
			console.log('new_bid_placed')
			io.emit('new_bid_placed', bid)
		})
	)
}



const socketsHandler = (io) => fromEvent(io, 'connection').pipe(
	map(initConnection(io)),
	mergeAll(),
).subscribe()



module.exports = socketsHandler
