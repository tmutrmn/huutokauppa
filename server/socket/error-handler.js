const { throwError } = require('rxjs')


const errorHandler = (socket) => (error, caught) => {
		console.log(error, caught)
  	socket.emit('new_bid_error', { message: error.message })
		return caught
		// return throwError(`New Bid Error: ${error.message}`); // return another `error`
		// return throwError(error.message)
		// return reject(new Error(error.message))
}

module.exports = errorHandler
