const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)


app.use((req, res, next) =>
{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    next();
})


app.use(express.static('public'))
app.use(bodyParser.json())
app.use('/api', require('./routes'))
app.use(require('./middleware/error-handler'))


server.listen(8000)

require('./socket')(io)
